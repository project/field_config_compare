(function ($, Drupal) {

  'use strict';

  /**
   * Gives configuration groups a number to identify unique configurations.
   */
  Drupal.behaviors.fieldConfigCompareIdentifyConfigVariations = {
    attach(context) {
      const variations = new Map;

      /**
       * Calculate the variation number.
       *
       * Variations are based on the hash and are unique per field /
       * configuration group combination
       *
       * @param {string} type
       *   The type of the configuration group.
       * @param {string} hash
       *   The group hash, identifying unique item values.
       *
       * @returns {int}
       *   The variation number, starting at 1.
       */
      const getVariationNumber = function (type, hash) {
        if (variations.get(type)) {
          const groupVariations = variations.get(type);
          if (groupVariations.get(hash)) {
            return groupVariations.get(hash);
          }
          else {
            groupVariations.set(hash, groupVariations.size + 1);
            return groupVariations.get(hash);
          }
        }
        else {
          const groupVariations = new Map;
          variations.set(type, groupVariations.set(hash, 1));
          return 1;
        }
      }

      /**
       * Clear the variation data.
       */
      const resetVariations = function () {
        variations.clear();
      }

      $('.js-fcc-field', context).each(function () {
        $(this).find('.js-fcc-group[data-fcc-level="1"]').each(function () {
          const $group = $(this);
          if (!$group.hasClass('has-variation')) {
            const groupHash = $(this).data('fcc-group-hash');
            const groupType = $(this).data('fcc-group');
            const variation = getVariationNumber(groupType, groupHash);
            const $notification = $(`<span class="fcc-notification fcc-notification-inline">${variation}</span>`);
            $group.find('.fcc-group-label')
              .wrap('<div class="notification-wrapper"></div>')
              .append($notification);
            $group.addClass('has-variation');
          }
        });
        resetVariations();
      });

    }
  };

  /**
   * Handle interaction with the overview settings form.
   */
  Drupal.behaviors.fieldConfigCompareSettingsForm = {
    attach(context) {
      const $form = $('.field-config-compare-overview-settings', context);
      if (!$form.length) {
        return;
      }

      this.timeoutID = undefined;

      /**
       * Show or hide a group.
       *
       * @param groupType
       *   The group type.
       * @param status
       *   True to display the group, false to hide.
       */
      const showGroup = function (groupType, status) {
        const $groups = $(`.js-fcc-group[data-fcc-group="${groupType}"]`, context);
        if (status) {
          $groups.show();
        }
        else {
          $groups.hide();
        }
      }

      /**
       * Hide groups that are equal in all field instances.
       *
       * A placeholder is added to the parent group if all items are
       * hidden.
       *
       * @param {boolean} hide
       *   True to hide equal items. False to show them.
       */
      const hideEqualGroups = function (hide) {
        $('.js-fcc-field', context).each(function () {
          const $field = $(this);

          if (!hide) {
            // @todo Respect current $input*Settings.
            $field.find('.js-fcc-group').show();
            return;
          }

          $field.find('.js-fcc-group[data-fcc-level="1"]').each(function () {
            const $group = $(this);

            // Skip this group if it is already hidden.
            if ($group.is(':hidden')) {
              return;
            }

            const groupType = $group.data('fcc-group');
            const groupHash = $group.data('fcc-group-hash');
            const $groupsOfSameType = $field.find(`.js-fcc-group[data-fcc-level="1"][data-fcc-group="${groupType}"]`);
            const $groupsWithSameHash = $groupsOfSameType.filter(`[data-fcc-group-hash="${groupHash}"]`);

            // If there is only one groups with this type, hide it.
            if ($groupsOfSameType.length === 1) {
              $group.hide();
              return;
            }

            // If not all groups have the same values (hash), don't hide them.
            if ($groupsOfSameType.length > $groupsWithSameHash.length) {
              return;
            }

            $groupsOfSameType.hide();
          });
        });
      }

      /**
       * Hide items that are equal in all field instances.
       *
       * @param {boolean} hide
       *   True to hide equal items, false to show them.
       */
      const hideEqualItems = function (hide) {
        $('.js-fcc-field', context).each(function () {
          const $field = $(this);

          if (!hide) {
            // @todo Respect current $input*Settings.
            $field.find('.js-fcc-items').show();
            $field.find('.js-fcc-item').show();
            return;
          }

          $field.find('.js-fcc-group[data-fcc-level="1"]').each(function () {
            const $group = $(this);

            // Skip this group if all items are already hidden.
            if ($group.find('> .js-fcc-items').is(':hidden')) {
              return;
            }

            // Find ANY child item (also at deeper levels) and hide if needed.
            $group.find('.js-fcc-item').each(function () {
              const $item = $(this);

              // Skip if the immediate parent group is hidden.
              if ($item.closest('.js-fcc-items').is(':hidden')) {
                return;
              }

              // Skip if this item is already hidden.
              if ($item.is(':hidden')) {
                return;
              }

              comparableItems($item).hide();

              // When the last item of a group gets hidden, we hide the group it
              // belongs too. Next, traverse upwards because the group may have
              // been the last item from its parent group too. Continue
              // recursively.
              let $parentGroup;
              let $traversalItem = $item;
              if ($traversalItem.is(':hidden')) {
                while ($traversalItem) {
                  if ($traversalItem.siblings(':visible').length === 0) {
                    $parentGroup = $item.closest('.js-fcc-group');
                    if ($parentGroup.is(':visible')) {
                      $parentGroup.hide();
                      // Stop before the top level group, which is covered by
                      // hideEqualGroups().
                      if ($parentGroup.data('fcc-level') > 1) {
                        $traversalItem = $parentGroup.closest('.js-fcc-item');
                        comparableItems($traversalItem).hide();
                      } else {
                        $traversalItem = false;
                      }
                    } else {
                      $traversalItem = false;
                    }
                  } else {
                    $traversalItem = false;
                  }
                }
              }
            });
          });
        });
      }

      /**
       * Return all items that are comparable with a given.
       *
       * Comparable items are implementation of the same field that have the
       * same name (property path) and the same value (including the value of
       * their children).
       *
       * @param $item
       * @returns {jQuery|HTMLElement}
       */
      const comparableItems = function ($item) {
        const itemId = $item.data('fcc-item-id');
        const itemHash = $item.data('fcc-item-hash');
        const $itemsWithSameId = $item.closest('.js-fcc-field')
          .find(`.js-fcc-item[data-fcc-item-id="${itemId}"]`);
        const $itemsWithSameHash = $itemsWithSameId.filter(`[data-fcc-item-hash="${itemHash}"]`);


        if ($itemsWithSameId.length > $itemsWithSameHash.length) {
          return $();
        }

        return $itemsWithSameId;
      }

      /**
       * Make height of adjacent groups (same field, same group type) equal.
       */
      const equalHeightGroups = function () {
        $('.js-fcc-field', context).each(function () {
          const maxHeights = new Map();
          const $field = $(this);

          // Measure the height of each groups and store the maximum value.
          $field.find('.js-fcc-group[data-fcc-level="1"]').each(function () {
            const $group = $(this);
            if ($group.is(':hidden')) {
              return;
            }

            $group.height('auto');
            const groupType = $group.data('fcc-group');
            const height = $(this).height();
            if (maxHeights.get(groupType)) {
              maxHeights.set(groupType, Math.max(height, maxHeights.get(groupType)));
            }
            else {
              maxHeights.set(groupType, height);
            }
          });

          // If all groups of a field are hidden, we can skip further
          // processing.
          if (maxHeights.size === 0) {
            return;
          }

          // Apply the maximum group height (per group type) to each group.
          $field.find('.js-fcc-group[data-fcc-level="1"]').each(function () {
            const $group = $(this);
            if ($group.is(':hidden')) {
              return;
            }

            const groupType = $group.data('fcc-group');
            $(this).height(maxHeights.get(groupType));
          });

          // We are done with this field. Clear the heights for the next round.
          maxHeights.clear();
        });
      }

      once('fieldConfigCompareSettingsForm', '.field-config-compare-overview-settings', context).forEach(function () {
        const $inputFieldSettings = $form.find('input[name="visible[field]"]');
        const $inputStorageSettings = $form.find('input[name="visible[storage]"]');
        const $inputWidgetSettings = $form.find('input[name="visible[widget]"]');
        const $inputFormatterSettings = $form.find('input[name="visible[formatter]"]');
        const $inputHideEqual = $form.find('input[name="hide_equal"]');
        const $inputFormMode = $form.find('select[name="form_mode"]');
        const $inputViewMode = $form.find('select[name="view_mode"]');
        const $submitButton = $form.find('.js-form-submit');

        // When settings change.
        $inputFieldSettings.on('change', function () {
          showGroup('field', $(this).is(':checked'));
          // @todo Update URL and stored settings.
        });
        $inputStorageSettings.on('change', function () {
          showGroup('storage', $(this).is(':checked'));
          // @todo Update URL and stored settings.
        });
        $inputWidgetSettings.on('change', function () {
          showGroup('widget', $(this).is(':checked'));
          // @todo Update URL and stored settings.
        });
        $inputFormatterSettings.on('change', function () {
          showGroup('formatter', $(this).is(':checked'));
          // @todo Update URL and stored settings.
        });
        $inputHideEqual.on('change', function () {
          hideEqualGroups($(this).is(':checked'));
          hideEqualItems($(this).is(':checked'));
          equalHeightGroups();
          // @todo Update URL and stored settings.
        });
        $inputFormMode.on('change', function () {
          $submitButton.click();
        });
        $inputViewMode.on('change', function () {
          $submitButton.click();
        });

        // Initial state.
        showGroup('field', $inputFieldSettings.is(':checked'));
        showGroup('storage', $inputStorageSettings.is(':checked'));
        showGroup('widget', $inputWidgetSettings.is(':checked'));
        showGroup('formatter', $inputFormatterSettings.is(':checked'));
        hideEqualGroups($inputHideEqual.is(':checked'));
        hideEqualItems($inputHideEqual.is(':checked'));
        equalHeightGroups();

        $submitButton.hide();

        // Make heights equal on window resize.
        window.addEventListener('resize', function (event) {
          if (typeof this.timeoutID === 'number') {
            clearTimeout(this.timeoutID);
          }

          this.timeoutID = setTimeout(equalHeightGroups, 500);
        });

      });
    }
  };

})(jQuery, Drupal);

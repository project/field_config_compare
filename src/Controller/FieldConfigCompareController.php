<?php

namespace Drupal\field_config_compare\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\field_config_compare\FieldConfigCompareManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Page and title callbacks for Field Config Compare routes.
 */
class FieldConfigCompareController extends ControllerBase {

  /**
   * The field config compare manager.
   *
   * @var \Drupal\field_config_compare\FieldConfigCompareManagerInterface
   */
  protected FieldConfigCompareManagerInterface $fieldConfigCompareManager;

  /**
   * Constructor.
   *
   * @param \Drupal\field_config_compare\FieldConfigCompareManagerInterface $fieldConfigCompareManager
   *   The field config compare manager.
   */
  public function __construct(FieldConfigCompareManagerInterface $fieldConfigCompareManager) {
    $this->fieldConfigCompareManager = $fieldConfigCompareManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): FieldConfigCompareController {
    return new static(
      $container->get('field_config_compare.manager')
    );
  }

  /**
   * Build an index page of field config comparisons pages.
   *
   * @return array
   *   Drupal render array.
   */
  public function indexPage(): array {

    $items = [];
    foreach ($this->entityTypeManager()->getDefinitions() as $definition) {
      if ($definition instanceof ContentEntityType
        && is_subclass_of($definition->getOriginalClass(), 'Drupal\Core\Entity\FieldableEntityInterface')
      ) {
        if ($this->hasFieldConfigs($definition->id())) {
          $items[] = Link::createFromRoute($definition->getLabel(), 'field_config_compare.entity_overview', ['entity_type' => $definition->id()]);
        }
      }
    }

    return [
      '#theme' => 'item_list',
      '#items' => $items,
      '#cache' => ['max-age' => 0],
    ];
  }

  /**
   * Builds a field comparison overview per entity type.
   *
   * @param string $entity_type
   *   The entity type to use.
   *
   * @return array
   *   Drupal render array.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   If the entity type is not found.
   */
  public function entityTypeOverviewPage(string $entity_type): array {

    $definition = $this->entityTypeManager()->getDefinition($entity_type, FALSE);
    if (empty($definition)) {
      throw new NotFoundHttpException();
    }
    if (!is_a($definition->getOriginalClass(), 'Drupal\Core\Entity\FieldableEntityInterface', TRUE)) {
      throw new NotFoundHttpException();
    }

    $build['form'] = [
      '#type' => 'details',
      '#title' => $this->t('Display settings'),
      '#open' => FALSE,
      'settings' => $this->formBuilder()->getForm('\Drupal\field_config_compare\Form\OverviewSettings'),
    ];
    ;
    $build['table'] = $this->fieldConfigCompareManager->buildEntityTypeTable($definition->id());
    return $build;
  }

  /**
   * Title callback for field comparison overview page.
   *
   * @param string $entity_type
   *   The entity type to use.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The title.
   */
  public function entityTypeOverviewTitle(string $entity_type): TranslatableMarkup {

    $definition = $this->entityTypeManager()->getDefinition($entity_type, FALSE);
    if (empty($definition)) {
      $this->t('Compare field configuration');
    }

    return $this->t('Compare fields of entity type @entity_type', ['@entity_type' => $definition->getLabel()]);
  }

  /**
   * Checks if an entity type has fields configured.
   *
   * @param string $entityType
   *   The entity type to check.
   *
   * @return bool
   *   True if so.
   */
  protected function hasFieldConfigs(string $entityType): bool {
    $fieldConfigs = $this->entityTypeManager->getStorage('field_config')
      ->loadByProperties([
        'entity_type' => $entityType,
      ]);

    return !empty($fieldConfigs);
  }

}

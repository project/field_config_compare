<?php

namespace Drupal\field_config_compare;

/**
 * The FieldConfigCompareManager service.
 */
interface FieldConfigCompareManagerInterface {

  /**
   * Builds a comparison table of field configurations.
   *
   * @param string $entityType
   *   The entity type for which to build the table.
   * @param string $formMode
   *   The form mode to use.
   * @param string $viewMode
   *   The view mode to use.
   *
   * @return array
   *   Drupal render array.
   */
  public function buildEntityTypeTable(string $entityType, string $formMode = 'default', string $viewMode = 'default'): array;

}
